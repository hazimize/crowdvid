from django import forms

class formulir_payment(forms.Form):
    Nominal = forms.IntegerField(widget=forms.TextInput(attrs={
        'class' : 'form-control',
        'placeholder' : 'Rp',
        'type' : 'number',
        'required' : True
    }))
    Nama = forms.CharField(widget=forms.TextInput(attrs={
        'class' : 'form-control',
        'placeholder' : 'Nama Lengkap',
        'type' : 'text',
        'required' : True
    }))
    Email = forms.CharField(widget=forms.TextInput(attrs={
        'class' : 'form-control',
        'placeholder' : 'Email Pribadi',
        'type' : 'text',
        'required' : True
    }))