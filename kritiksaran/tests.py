from django.test import TestCase, Client
from django.urls import resolve
from . import views
from .models import FormKritikSaran, Tampilan
from .forms import FormulirKritikSaran
# Create your tests here.

class KritikSaran(TestCase):
    def test_url_form_kritik_saran(self):
        response= Client().get('/kritik-saran/form-kritik-saran/')
        self.assertEqual(response.status_code,200)

    def test_form_pendaftar_using_kritik_saran_template(self):
        response=Client().get('/kritik-saran/form-kritik-saran/')
        self.assertTemplateUsed(response, 'formkritiksaran.html')

    def test_form_kritik_saran_using_form_kritik_saran_func(self):
        found= resolve('/kritik-saran/form-kritik-saran/')
        self.assertEqual(found.func, views.form_kritik_saran)

    def test_models_form_kritik_saran(self):
        orang=FormKritikSaran.objects.create(namainitial="Nanang", kritik="SKUD SKUD", saran="YOLO")
        counting_all_available_person = FormKritikSaran.objects.all().count()
        self.assertEqual(counting_all_available_person, 1)

    def test_form_kritik_saran(self):
        data = {'namainitial': "Nanang racing", 'kritik':"dah bagus", 'saran':'lanjutkan'}
        pendaftaran = FormulirKritikSaran(data)
        self.assertTrue(pendaftaran.is_valid())
        self.assertEqual(pendaftaran.cleaned_data['namainitial'], "Nanang racing")
        self.assertEqual(pendaftaran.cleaned_data['kritik'], "dah bagus")
        self.assertEqual(pendaftaran.cleaned_data['saran'], 'lanjutkan')
    
    # def test_konten_pendaftar_html(self):
    #     response = Client().get('/kritik-saran/form-kritik-saran/')
    #     isi = response.content.decode('utf8')
    #     self.assertIn("Kritik & Saran", isi)
    #     self.assertIn("Nama Initial", isi)
    #     self.assertIn("Kritik", isi)
    #     self.assertIn("Saran", isi)


    def test_url_display_kritik_saran(self):
        response= Client().get('/kritik-saran/')
        self.assertEqual(response.status_code,200)

    def test_display_kritik_saran(self):
        found= resolve('/kritik-saran/')
        self.assertEqual(found.func, views.display_kritik_saran)

    def test_display_kritiksaran_using_display_kritiksaran_template(self):
        response=Client().get('/kritik-saran/')
        self.assertTemplateUsed(response, 'displaykritiksaran.html')

    def test_konten_display_kritiksaran_html(self):
        response = Client().get('/kritik-saran/')
        isi = response.content.decode('utf8')
        self.assertIn("Kritik dan Saran", isi)
