from django import forms
from .models import Formulir
class PendaftaranForm(forms.ModelForm):
      class Meta:
        model = Formulir
        fields ='__all__'
        labels = {
          'namalengkap':('Nama Lengkap'),
          'email':('Email'),
          'pekerjaan':('Pekerjaan'),
          'kota':('Kota'),
          'jmldana':('Jumlah Dana'),
          'ceritasingkat':('Cerita Singkat'),
        }
        placeholder= {
          
        }


