from django.test import TestCase, Client
from django.urls import resolve
from . import views
from .models import Formulir
from .forms import PendaftaranForm
# Create your tests here.
class FormPendaftar(TestCase):
    def test_url_form_pendaftar(self):
        response= Client().get('/penerima/')
        self.assertEqual(response.status_code,200)
    def test_form_pendaftar_using_pendaftar_template(self):
        response=Client().get('/penerima/')
        self.assertTemplateUsed(response, 'form/pendaftar.html')
    def test_form_pendaftar_using_pendaftar_func(self):
        found= resolve('/penerima/')
        self.assertEqual(found.func, views.pendaftar_form)
    def test_models_form_pendaftar(self):
        orang=Formulir.objects.create(namalengkap="Nanang racing",email="nanang@racing.com", pekerjaan="pembalap", kota="tangsel", jmldana=15000, ceritasingkat="saya sedih")
        counting_all_available_person = Formulir.objects.all().count()
        self.assertEqual(counting_all_available_person,1)
    def test_form_pendaftar(self):
        data = {'namalengkap': "Nanang racing", 'email':"nanang@racing.com", 'pekerjaan':'pembalap','kota':'tangsel','jmldana': 15000,'ceritasingkat':'saya sedih'}
        pendaftaran = PendaftaranForm(data)
        self.assertTrue(pendaftaran.is_valid())
        self.assertEqual(pendaftaran.cleaned_data['namalengkap'], "Nanang racing")
        self.assertEqual(pendaftaran.cleaned_data['email'], "nanang@racing.com")
        self.assertEqual(pendaftaran.cleaned_data['pekerjaan'], 'pembalap')
        self.assertEqual(pendaftaran.cleaned_data['kota'], 'tangsel')
        self.assertEqual(pendaftaran.cleaned_data['jmldana'], 15000)
        self.assertEqual(pendaftaran.cleaned_data['ceritasingkat'], 'saya sedih')
    #def test_konten_pendaftar_html(self):
    #    response = Client().get('/penerima/')
    #    isi = response.content.decode('utf8')
    #    self.assertIn("Peraturan", isi)
    #    self.assertIn("Lengkapi data di bawah ini", isi)
    #    self.assertIn("Nama Lengkap", isi)
    #    self.assertIn("Email", isi)
    #    self.assertIn("Pekerjaan", isi)
    #    self.assertIn("Kota", isi)
    #    self.assertIn("Jumlah Dana", isi)
    #    self.assertIn("Cerita Singkat", isi)


    def test_url_display_penerima(self):
        response= Client().get('/penerima/donatur/')
        self.assertEqual(response.status_code,200)
    def test_display_penerima(self):
        found= resolve('/penerima/donatur/')
        self.assertEqual(found.func, views.display_form)
    def test_display_penerima_using_display_penerima_template(self):
        response=Client().get('/penerima/donatur/')
        self.assertTemplateUsed(response, 'form/displaypenerima.html')
    def test_konten_display_penerima_html(self):
        response = Client().get('/penerima/donatur/')
        isi = response.content.decode('utf8')
        self.assertIn("Mereka butuh bantuan finansialmu!", isi)


    def test_url_terimakasih(self):
        response= Client().get('/penerima/terimakasih/')
        self.assertEqual(response.status_code,200)
    def test_thanks(self):
        found= resolve('/penerima/terimakasih/')
        self.assertEqual(found.func, views.thanks)
    def test_konten_terimakasih(self):
        response = Client().get('/penerima/terimakasih/')
        isi = response.content.decode('utf8')
        self.assertIn("Terima Kasih!", isi)
        self.assertIn("Bantuan finansial kamu akan segera kamu dapat dalam waktu dekat.",isi)