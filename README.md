# Tugas Kelompok 1

[![pipeline status][pipeline-badge]][commits-gl]
[![coverage report][coverage-badge]][commits-gl]

## Nama Kelompok C-13

1. Achmad Ghifari Taufiqurrachman (1906399556)
2. Dewinta Dyah Maharani (1906399562)
3. Muhammad Hazim Al Farouq (1906400103)
4. Robby (1906400381)

## Link

-  [**Herokuapp**][link-heroku]
-  [**Wireframe**][link-wireframe]
-  [**Prototype**][link-prototype]

## Aplikasi Kami

Aplikasi kami merupakan platform crowdfunding khusus bagi mereka yang terkena dampak ekonomi dari pandemi COVID-19. Diharapkan dengan adanya platform kami juga dapat meningkatkan kepedulian masyarakat akan pentingnya peduli terhadap sesama dan meningkatkan solidaritas dalam masa krisis.

## Fitur-Fitur

Pada aplikasi kami, terdapat:
-  <b>Homepage</b> berisi infografis serta testimoni para penerima bantuan.
-  <b>Form Pembayaran</b> untuk donatur, dengan instruksi pembayaran.
-  <b>Formulir Pendaftaran</b> bagi calon penerima dana yang ingin mendaftar.
-  <b>Calon Penerima Dana</b> menu berisi informasi para calon penerima dana.
-  <b>Halaman Terimakasih</b> untuk pendaftar dan donatur.
-  <b>Halaman Kritik dan Saran</b> sebagai tempat untuk memberikan kritik dan saran untuk CROWD-VID.
-  <b>Halaman Testiomoni</b> yang berisi pengalaman pengguna web CROWD-VID baik penerima maupun donatur.


[pipeline-badge]: https://gitlab.com/laymonage/django-template-heroku/badges/master/pipeline.svg
[coverage-badge]: https://gitlab.com/laymonage/django-template-heroku/badges/master/coverage.svg
[commits-gl]: https://gitlab.com/c-13/tugas-kelompok-1-ppw/-/commits/master
[link-heroku]: https://tk1-ppw-c13.herokuapp.com/
[link-prototype]: https://www.figma.com/file/3YasDXJcsLR3IZPQsa7jmp/Untitled?node-id=0%3A1
[link-wireframe]: https://wireframe.cc/pro/pp/b4fd56d54384635
