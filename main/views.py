from django.shortcuts import render
from django.contrib.auth import authenticate, login, logout
from django.http import HttpResponse, HttpResponseRedirect
from django.contrib.auth.models import User
from django.contrib.auth.forms import UserCreationForm


def log_out(request):
    logout(request)
    return HttpResponseRedirect('/')
    
def home(request):
    return render(request, 'main/home.html')

def signup(request):
    if(request.method == "POST"):
        name = request.POST['username']
        email = request.POST['email']
        passs = request.POST['password']
        user = User.objects.create_user(name, email, passs)
        user.save()
        return HttpResponseRedirect('/login')
    else:
        return render(request, 'main/signup.html')


def log_in(request):
    if (request.method == "POST"):
        name = request.POST['username']
        passs = request.POST['password']
        user = authenticate(request, username=name, password=passs)
        success = False
        if user is not None:
            login(request, user)
            success = True
            return render(request, 'main/home.html', {'success':success})

        else:

            return render(request, 'main/login.html', {'success':success})
    else:  
        
        return render(request, 'main/login.html')


