from django.db import models

# Create your models here.
class Testimoni(models.Model):
    nama = models.CharField(blank=False, max_length= 30)
    pekerjaan = models.CharField(blank=False, max_length= 20)
    ulasan = models.CharField(blank=False, max_length= 150)
   
